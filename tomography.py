import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(precision=2)

N = 5
BG = 2743.153450757964
BG_ERROR = 17.458374147153453



def weighted_mean(values, errors):
    return np.sum(values / errors ** 2) / np.sum(1 / errors ** 2)


def weighted_mean_error(errors):
    return 1. / np.sqrt(np.sum(1. / errors ** 2))


# Order = 90, -90, 0, 45, -45
def index(angle):
    if angle == 90:
        return 0
    elif angle == -90:
        return 1
    elif angle == 0:
        return 2
    elif angle == 45:
        return 3
    elif angle == -45:
        return 4
    else:
        return None

def read(state='P'):
    dataframe = np.empty([2, N, N])
    if state == 'H':
        file = 'data/5.6_TOM-StateB.csv'
    elif state == 'P':
        file = 'data/5.6_TOM-StateA.csv'

    csv = np.genfromtxt(file, dtype=np.int32, delimiter=',', skip_header=3)
    qs = csv[:, 0]
    ps = csv[:, 1]
    ys = [csv[:, 2], csv[:, 3]]

    for channel in [0, 1]:
        for i in range(len(csv)):
            q = qs[i]
            p = ps[i]
            y = ys[channel][i]
            if index(q) is not None:
                dataframe[channel][index(q)][index(p)] = y

    return dataframe

def calc(data):
    errors = np.sqrt(data)

    # Correct for background
    data = data - BG
    errors = np.sqrt(errors**2 + BG_ERROR**2)

    H = data[0:3, 0:2].flatten()
    He = errors[0:3, 0:2].flatten()
    V = data[0:3, 2].flatten()
    Ve = errors[0:3, 2].flatten()
    P = data[3:5, 3].flatten()
    Pe = errors[3:5, 3].flatten()
    M = data[3:5, 4].flatten()
    Me = errors[3:5, 4].flatten()

    R = np.array([
        data[0][4],
        data[1][4],
        data[2][3],
        data[3][1],
        data[4][2],
        data[3][0]
    ])
    Re = np.array([
        errors[0][4],
        errors[1][4],
        errors[2][3],
        errors[3][1],
        errors[4][2],
        errors[3][0]
    ])

    L = np.array([
        data[0][3],
        data[1][3],
        data[2][4],
        data[3][2],
        data[4][1],
        data[4][0]
    ])
    Le = np.array([
        errors[0][3],
        errors[1][3],
        errors[2][4],
        errors[3][2],
        errors[4][1],
        errors[4][0]
    ])

    # Total counts
    H_averaged = 0.5*(np.array([H[0], H[2], H[4]]) + np.array([H[1], H[3], H[5]]))
    He_averaged = (np.array([He[0], He[2], He[4]]) + np.array([He[1], He[3], He[5]]))/np.sqrt(2)
    NH = H_averaged + V
    NP = P + M
    NR = R[:-1] + L[:-1]
    NHe = np.sqrt(He_averaged**2 + Ve**2)
    NPe = np.sqrt(Pe**2 + Me**2)
    NRe = np.sqrt(Re[:-1]**2 + Le[:-1]**2)
    Ns = np.array([weighted_mean(NH, NHe), weighted_mean(NP, NPe), weighted_mean(NR, NRe)])
    N_errors = np.array([weighted_mean_error(NHe), weighted_mean_error(NPe), weighted_mean_error(NRe)])
    print('H + V', Ns[0], N_errors[0])
    print('+ , -', Ns[1], N_errors[1])
    print('R + L', Ns[2], N_errors[2])
    print('Mean', weighted_mean(Ns, N_errors), np.sqrt(np.var(Ns))/np.sqrt(2))

    # calc
    #for psi, err in [(H, He), (V, Ve), (P, Pe), (M, Me), (R, Re), (L, Le)]:
    for psi, err in [(H, He), (P, Pe), (R, Re), (V, Ve), (M, Me), (L, Le)]:
        print(
            weighted_mean(psi, err)/1000,
            np.max([np.sqrt(np.var(psi))/np.sqrt(len(psi)-1), weighted_mean_error(err)])/1000
        )
    """
    norm = np.mean(H) + np.mean(V)
    s0 = 1
    s1 = 2*np.mean(P)/norm - 1
    s2 = 2*np.mean(R)/norm - 1
    s3 = 2*np.mean(H)/norm - 1
    """
    norm = weighted_mean(Ns, N_errors)
    s0 = 1
    """
    s1s = np.concatenate(2*weighted_mean(P, Pe) - 1, 1 - 2*weighted_mean(M, Me)) / weighted_mean(NP, NPe)
    s2s = np.concatenate(2*weighted_mean(R, Re) - 1, 1 - 2*weighted_mean(L, Le)) / weighted_mean(NR, NRe)
    s3s = np.concatenate(2*weighted_mean(H, He) - 1, 1 - 2*weighted_mean(V, Ve)) / weighted_mean(NH, NHe)
    """

    s1s = np.concatenate([2*P / NP - 1, 1 - 2*M / NP])
    s2s = np.concatenate([2*R[:-1] / NR - 1, 1 - 2 * L[:-1] / NR])
    s3s = np.concatenate([2*H_averaged / NH - 1, 1 - 2 * V / NH])
    s1_errors = s1s * np.sqrt(np.concatenate([
        (Pe / P) ** 2 + (Pe / NP) ** 2 + (Me / NP) ** 2,
        (Me / M) ** 2 + (Pe / NP) ** 2 + (Me / NP) ** 2
    ]))
    s2_errors = s2s * np.sqrt(np.concatenate([
        (Re[:-1] / R[:-1]) ** 2 + (Re[:-1] / NR) ** 2 + (Le[:-1] / NR) ** 2,
        (Le[:-1] / L[:-1]) ** 2 + (Re[:-1] / NR) ** 2 + (Le[:-1] / NR) ** 2
    ]))
    s3_errors = s3s * np.sqrt(np.concatenate([
        (He_averaged / H_averaged) ** 2 + (He_averaged / NH) ** 2 + (Ve / NH) ** 2,
        (Ve / V) ** 2 + (He_averaged / NH) ** 2 + (Ve / NH) ** 2
    ]))
    s1 = weighted_mean(s1s, s1_errors)
    s2 = weighted_mean(s2s, s2_errors)
    s3 = weighted_mean(s3s, s3_errors)
    print()
    print('S1', s1, weighted_mean_error(s1_errors), np.sqrt(np.var(s1s))/np.sqrt(len(s1s) - 1))
    print('S2', s2, weighted_mean_error(s2_errors), np.sqrt(np.var(s2s))/np.sqrt(len(s2s) - 1))
    print('S3', s3, weighted_mean_error(s3_errors), np.sqrt(np.var(s3s))/np.sqrt(len(s3s) - 1))

    rho11 = (1+s3)/2
    rho22 = (1-s3)/2
    rho12 = s1/2 - (s2/2)*1j
    rho21 = s1/2 + (s2/2)*1j
    print(rho11, rho12)
    print(rho21, rho22)
    print('F (horizontal) = ', rho11)
    print('F (diagonal) = ', 0.5*(rho11+rho22) + np.sqrt(rho11*rho22 - rho12*rho21))
    sabs = np.sqrt(s1**2 + s2**2 + s3**2)

    return s0, s1, s2, s3, sabs, norm

def plot_rho_re(s0, s1, s2, s3, ax):
    rho11 = .5*(s0 + s3)
    rho22 = .5*(s0 - s3)
    rho12 = .5*s1
    plot_rho(rho11, rho12, rho12, rho22, ax)

def plot_rho_im(s0, s1, s2, s3, ax):
    rho11 = 0
    rho22 = 0
    rho12 = -.5*s2
    rho21 = .5*s2
    plot_rho(rho11, rho12, rho21, rho22, ax)

def plot_rho(rho11, rho12, rho21, rho22, ax):

    # xy axis
    _x = np.arange(2)
    _y = np.arange(2)
    _xx, _yy = np.meshgrid(_x, _y)
    xx, yy = _xx.ravel(), _yy.ravel()
    width = depth = 0.4
    xx = xx + (1 - width)/2
    yy = yy + (1 - depth)/2

    # 3d plot
    ax.set_xlim([0, 2])
    ax.set_ylim([0, 2])
    if state == 'P':
        ax.set_zlim([0,0.6])
    elif state == 'H':
        ax.set_zlim([0, 1])
    ax.set_xticks([0.5, 1.5])
    ax.set_xticklabels(['H', 'V'])
    ax.set_yticks([0.5, 1.5])
    ax.set_yticklabels(['V', 'H'])

    zz = [rho21, rho22, rho11, rho12]
    bottom = np.zeros_like(zz)
    ax.bar3d(xx, yy, bottom, width, depth, zz, color='white', edgecolor='black', shade=True)
    ax.grid(False, axis='y')

figsize=(183/25.4, 160/25.4)

print('Horizontal')
state = 'H'
fig = plt.figure(figsize=figsize)
fig.tight_layout()
data = read(state=state)

s0, s1, s2, s3, sabs, n = calc(data[0])
ax = fig.add_subplot(221, projection='3d')
plot_rho_re(s0, s1, s2, s3, ax)
ax = fig.add_subplot(223, projection='3d')
plot_rho_im(s0, s1, s2, s3, ax)

s0, s1, s2, s3, sabs, n = calc(data[1])
ax = fig.add_subplot(222, projection='3d')
plot_rho_re(s0, s1, s2, s3, ax)
ax = fig.add_subplot(224, projection='3d')
plot_rho_im(s0, s1, s2, s3, ax)

plt.subplots_adjust(
    left=-0.02,
    right=0.98,
    bottom=0.05,
    top=1.00
)
fig.savefig('tomA.pdf')
plt.close()

print('\nDiagonal')
state = 'P'
fig = plt.figure(figsize=figsize)
data = read(state=state)

s0, s1, s2, s3, sabs, n = calc(data[0])
ax = fig.add_subplot(221, projection='3d')
plot_rho_re(s0, s1, s2, s3, ax)
ax = fig.add_subplot(223, projection='3d')
plot_rho_im(s0, s1, s2, s3, ax)

s0, s1, s2, s3, sabs, n = calc(data[1])
ax = fig.add_subplot(222, projection='3d')
plot_rho_re(s0, s1, s2, s3, ax)
ax = fig.add_subplot(224, projection='3d')
plot_rho_im(s0, s1, s2, s3, ax)

plt.subplots_adjust(
    left=-0.02,
    right=0.98,
    bottom=0.05,
    top=1.00
)
fig.savefig('tomB.pdf')
