import numpy as np
import matplotlib.pyplot as plt



# Read csv
csv = np.genfromtxt('data/5.5_Interferometer_plus_Beamsplitter.csv', delimiter=';')
x = csv[3:, 0]

ys = [
    csv[3:, 1],
    csv[3:, 2],
    csv[3:, 3]
]
inside_single = csv[3, 1:4]
outside_single = csv[5, 1:4]

inside_double = csv[3, 4:7]*20
outside_double = csv[5, 4:7]*20

inside_triple = [csv[3][7]*1000]
outside_triple = [csv[5][7]*1000]

fig, ax = plt.subplots()

width = 0.4

x = np.asarray([1, 2, 3, 4, 5, 6, 7])

# Single rates
mean_single = np.concatenate((inside_single, outside_single)).mean()
mean_double = np.concatenate((inside_double, outside_double)).mean()
mean_triple = np.concatenate((inside_triple, outside_triple)).mean()

inside = np.concatenate((inside_single/mean_single, inside_double/mean_double, inside_triple/mean_triple))
outside = np.concatenate((outside_single/mean_single, outside_double/mean_double, outside_triple/mean_triple))
inside = np.concatenate((inside_single, inside_double, inside_triple))
outside = np.concatenate((outside_single, outside_double, outside_triple))

fig, ax = plt.subplots(figsize=(136/25.4, 89/16*9/25.4))
plt.bar(x, height=inside/1000, width=width, color='black', edgecolor='black', label='Inside dip')
plt.bar(x+width, height=outside/1000, width=width, color='#CCCCCC', edgecolor='black', label='Outside dip')
plt.xticks([1+width/2, 2+width/2, 3+width/2, 4+width/2, 5+width/2, 6+width/2, 7+width/2], ['t', '1', '2', 't1', 't2', '12', 't12'])
plt.yticks([0, 25, 50])
plt.ylabel('Rate ($\\mathrm{kHz}$)')
plt.legend()
plt.savefig('hom_bs.pdf')
plt.close()

# Double coincidences
