import numpy as np
import matplotlib.pyplot as plt
from analyzer import Data, Figure

TIME_WINDOW = 30e-9


def weighted_mean(values, errors):
    return np.sum(values / errors ** 2) / np.sum(1 / errors ** 2)


def weighted_mean_error(errors):
    return 1. / np.sqrt(np.sum(1. / errors ** 2))


def reflectance(a, b):
    return a/(a + b)


def reflectance_error(a, b, a_error, b_error):
    return reflectance(a, b) * np.sqrt((a_error/a)**2 + (a_error/(a+b))**2 + (b_error/(a+b))**2)


def str_reflectance(a, b, a_error, b_error):
    for i in range(len(a)):
        print(f'{reflectance(a, b)[i]*100:.2f} ± {reflectance_error(a, b, a_error, b_error)[i]*100:.2f} %')


# Read csv
csv = np.genfromtxt('data/5.3_HBT-count-rates.csv', delimiter=';')
x = csv[3:, 0]

ys = [
    csv[3:, 1],
    csv[3:, 2],
    csv[3:, 3]
]
etas = []
eta_errors = []
y_errors_raw = np.sqrt(ys)
print(f'Reflectances with bg')
str_reflectance(ys[1], ys[2], y_errors_raw[1], y_errors_raw[2])
etas.append(reflectance(ys[1], ys[2])[:3])
etas.append(reflectance(ys[1], ys[2])[3:])
eta_errors.append(reflectance_error(ys[1], ys[2], y_errors_raw[1], y_errors_raw[2])[:3])
eta_errors.append(reflectance_error(ys[1], ys[2], y_errors_raw[1], y_errors_raw[2])[3:])

print(f'Reflectance of bg')
str_reflectance(
    np.array([weighted_mean(ys[1][0:3], y_errors_raw[1][0:3])]),
    np.array([weighted_mean(ys[2][0:3], y_errors_raw[2][0:3])]),
    np.array([weighted_mean_error(y_errors_raw[1][0:3])]),
    np.array([weighted_mean_error(y_errors_raw[2][0:3])])
)

# Estimate background for tomography
bgs = []
bg_errors = []
for i in range(3):
    for j in range(3):
        bgs.append(ys[i][j])
        bg_errors.append(y_errors_raw[i][j])
bgs = np.asarray(bgs)
bg_errors = np.asarray(bg_errors)
print('BACKGROUNDS:', weighted_mean(bgs, bg_errors))
print('Error:', weighted_mean_error(bg_errors))

# Remove background
y_errors = []
remove_background = True
for i in range(3):
    if remove_background is True:
        bg = weighted_mean(ys[i][0:3], y_errors_raw[i][0:3])
        ys[i] = ys[i] - bg
        # Error propagation
        background_error = weighted_mean_error(y_errors_raw[i][0:3])
        y_errors_raw[i] = np.sqrt(y_errors_raw[i]**2 + background_error**2)

    # We don't need 0 mA and 10 mA
    ys[i] = ys[i][2:]
    y_errors.append(y_errors_raw[i][2:])
x = x[2:]

print(f'Reflectances without bg')
str_reflectance(ys[1], ys[2], y_errors[1], y_errors[2])
etas.append(reflectance(ys[1], ys[2])[1:])
eta_errors.append(reflectance_error(ys[1], ys[2], y_errors[1], y_errors[2])[1:])


kwargs = [
    {'color': 'black', 'label': 'Herald'},
    {'color': '#3C5488', 'label': 'Detector 1'},
    {'color': '#00A087', 'label': 'Detector 2'}
]
#    {'color': '#8491B4', 'label': 'Detector 2'}
kwargs2 = [
    {'color': '#3C5488', 'label': '$t\\leftrightarrow 1$'},
    {'color': '#00A087', 'label': '$t\\leftrightarrow 2$'}
]
kwargs3 = [
    {'color': '#8491B4', 'label': '$1\\leftrightarrow 2$'},
    {'color': 'black', 'label': '$t\\leftrightarrow 1 \\leftrightarrow 2$'}
]


def render_figure(fig, name):
    fig.xlabel = 'Pump current $(\\mathrm{mA})$'

    _ax = fig.ax
    _ax.axhline(0, color='black', linewidth=1)
    _ax.axvline(25.265, color='#8491B4', linewidth=1)

    yy = np.linspace(-50, 200, 2*8)
    _ax.fill_betweenx(yy, 24.89, 25.32, linewidth=0, color='#8491B4', alpha=0.2)

    fig.plot()
    _ax.set_xlim(19.5, 50.5)
    plt.legend(loc='upper left')
    plt.savefig(f'{name}.pdf')


# Single incidences
figure = Figure(width='single', residuals=False)
for i in range(3):
    n = Data(x, ys[i], yerr=y_errors[i], **kwargs[i])
    figure.add(n)
figure.ylabel = 'Incidence $(\\mathrm{kHz})$'
figure.ax.set_ylim(0, 150)
figure.ax.text(25.8, 5, '$I_\\mathrm{th}$')

render_figure(figure, 'hbt_single')


etas_heralded = {}
eta_errors_heralded = {}


# Coincidences
def coincidences(correct=False):

    if correct is False:
        print('Raw data coincidences')
        y_t1 = csv[5:, 4]
        y_t2 = csv[5:, 5]
        y_12 = csv[5:, 6]
        y_t12 = csv[5:, 7]
        y_t1_error = np.sqrt(y_t1)
        y_t2_error = np.sqrt(y_t2)
        y_12_error = np.sqrt(y_12)
        y_t12_error = np.sqrt(y_t12)

    elif correct is True:
        print('Coincidences corrected for false counts')
        """
        y_t1 = csv[5:, 4] - ys[0]*ys[1]*TIME_WINDOW
        y_t2 = csv[5:, 5] - ys[0]*ys[2]*TIME_WINDOW
        y_12 = csv[5:, 6] - ys[1]*ys[2]*TIME_WINDOW
        y_t12 = csv[5:, 7] - ys[0]*ys[1]*ys[2]*TIME_WINDOW**2
        y_t1_error = np.sqrt(csv[5:, 4] + (ys[0]*ys[1]*TIME_WINDOW)**2*(y_errors[0]**2/ys[0]**2 + y_errors[1]**2/ys[1]**2))
        y_t2_error = np.sqrt(csv[5:, 5] + (ys[0]*ys[2]*TIME_WINDOW)**2*(y_errors[0]**2/ys[0]**2 + y_errors[2]**2/ys[2]**2))
        y_12_error = np.sqrt(csv[5:, 6] + (ys[1]*ys[2]*TIME_WINDOW)**2*(y_errors[1]**2/ys[1]**2 + y_errors[2]**2/ys[2]**2))
        y_t12_error = np.sqrt(csv[5:, 7] + (ys[0]*ys[1]*ys[2]*TIME_WINDOW**2)**2*(y_errors[0]**2/ys[0]**2 + y_errors[1]**2/ys[1]**2 + y_errors[2]**2/ys[2]**2))
        print(ys[0])
        print(csv[5:, 0])
        """
        y_t1 = csv[5:, 4] - csv[5:, 1]*csv[5:, 2]*TIME_WINDOW
        y_t2 = csv[5:, 5] - csv[5:, 1]*csv[5:, 3]*TIME_WINDOW
        y_12 = csv[5:, 6] - csv[5:, 2]*csv[5:, 3]*TIME_WINDOW
        y_t12 = csv[5:, 7] - csv[5:, 1]*csv[5:, 2]*csv[5:, 3]*TIME_WINDOW**2
        y_t1_error = np.sqrt(csv[5:, 4] + (csv[5:, 0]*csv[5:, 1]*TIME_WINDOW)**2*(y_errors_raw[0][2:]**2/csv[5:, 0]**2 + y_errors_raw[1][2:]**2/csv[5:, 1]**2))
        y_t2_error = np.sqrt(csv[5:, 5] + (csv[5:, 0]*csv[5:, 2]*TIME_WINDOW)**2*(y_errors_raw[0][2:]**2/csv[5:, 0]**2 + y_errors_raw[2][2:]**2/csv[5:, 2]**2))
        y_12_error = np.sqrt(csv[5:, 6] + (csv[5:, 1]*csv[5:, 2]*TIME_WINDOW)**2*(y_errors_raw[1][2:]**2/csv[5:, 1]**2 + y_errors_raw[2][2:]**2/csv[5:, 2]**2))
        y_t12_error = np.sqrt(csv[5:, 7] + (csv[5:, 0]*csv[5:, 1]*csv[5:, 2]*TIME_WINDOW**2)**2*(y_errors_raw[0][2:]**2/csv[5:, 0]**2 + y_errors_raw[1][2:]**2/csv[5:, 1]**2 + y_errors_raw[2][2:]**2/csv[5:, 2]**2))

    print(f'Heralded reflectance')
    str_reflectance(y_t1, y_t2, y_t1_error, y_t2_error)
    etas_heralded[correct] = reflectance(y_t1, y_t2)
    eta_errors_heralded[correct] = reflectance_error(y_t1, y_t2, y_t1_error, y_t2_error)

    # Non-zero coincidences
    figure = Figure(width='single', residuals=False)
    figure.ylabel = 'Coincidence $(\\mathrm{kHz})$'
    n = Data(x, y_t1, yerr=y_t1_error, **kwargs2[0])
    figure.add(n)
    n = Data(x, y_t2, yerr=y_t2_error, **kwargs2[1])
    figure.add(n)
    figure.ax.set_ylim(0, 6)
    figure.ax.text(25.8, 0.2, '$I_\\mathrm{th}$')

    if correct is False:
        render_figure(figure, 'hbt_double')
    elif correct is True:
        render_figure(figure, 'hbt_corrected_double')

    # The rate is zero. Instead we simply determine
    # what would have happened with 1 Hz
    # to estimate a systematic error
    y_12[0] = 0.5
    y_t12[0] = 0.5

    gs = y_12 / ys[1] / ys[2] / TIME_WINDOW
    # Error propagation
    g_errors = gs * np.sqrt((y_12_error/y_12)**2 + (y_errors[1]/ys[1])**2 + (y_errors[2]/ys[2])**2)

    # Statistics
    print(f'20 mA: g(0) = 0')
    print(f'𝜎g = {y_12[0] / ys[1][0] / ys[2][0] / TIME_WINDOW / np.sqrt(12):.1f}')
    print(f'30 mA: g(0) = {gs[1]:.3f} ± {g_errors[1]:.3f}')
    print(f'40 mA: g(0) = {gs[2]:.3f} ± {g_errors[2]:.3f}')
    print(f'50 mA: g(0) = {gs[3]:.3f} ± {g_errors[3]:.3f}')
    print()

    # Zero coincidences
    figure = Figure(width='single', residuals=False)
    figure.ylabel = 'Coincidence $(\\mathrm{kHz})$'
    n = Data(x, y_12, yerr=y_12_error, **kwargs3[0])
    figure.add(n)
    n = Data(x, y_t12, yerr=y_t12_error, **kwargs3[1])
    figure.add(n)
    figure.ax.set_ylim(0, 0.26)
    figure.ax.text(25.8, 0.01, '$I_\\mathrm{th}$')

    if correct is False:
        render_figure(figure, 'hbt_triple')
    elif correct is True:
        render_figure(figure, 'hbt_corrected_triple')

    ghs = y_t12 * ys[0] / y_t1 / y_t2
    # Error propagation
    gh_errors = ghs * np.sqrt((y_t12_error/y_t12)**2 + (y_errors[0]/ys[0])**2 + (y_t1_error/y_t1)**2 + (y_t2_error/y_t2)**2)

    # Statistics
    print(f'20 mA: g_h(0) = 0.0')
    print(f'𝜎g_digit = {ghs[0] / np.sqrt(12):.1f}')
    print(f'30 mA: g_h(0) = {ghs[1]:.3f} ± {gh_errors[1]:.3f}')
    print(f'40 mA: g_h(0) = {ghs[2]:.3f} ± {gh_errors[2]:.3f}')
    print(f'50 mA: g_h(0) = {ghs[3]:.3f} ± {gh_errors[3]:.3f}')


coincidences()
print('\n\n')
coincidences(correct=True)

#
# Reflectances
#
Figure.y_scale = 1

kwargs = [
    {'color': 'black', 'label': 'Backgr.'},
    {'color': '#3C5488', 'marker': '_'},
    {'color': '#3C5488', 'label': 'Signal'},
    {'color': '#00A087', 'label': 'Heralded'}
    # #8491B4
]
kwargs_line = [
    {'color': 'black', 'label': 'W. means'},
    {'color': '#3C5488'},
    {'color': '#3C5488'},
    {'color': '#00A087'}
    # #8491B4
]

figure = Figure(width='single', residuals=False)
ax = figure.ax
figure.x_major_locator = 10
figure.x_minor_locator = 100
etas.append(.5*(etas_heralded[True][1:] + etas_heralded[False][1:]))
eta_errors.append(eta_errors_heralded[True][1:])

delta = [-1, 0, 0, 1]
for i in range(4):
    wmean = weighted_mean(etas[i], eta_errors[i])*100
    print('Eta:', wmean)
    print('Error:', weighted_mean_error(eta_errors[i])*100)
    print('Visibility:', 100./(100**2/(wmean*(100-wmean)) - 3.))
    n = Data(np.array([0, 10, 20]) + delta[i], 100*etas[i], yerr=100*eta_errors[i], **kwargs[i])
    if i != 1:
        ax.axhline(wmean, **kwargs_line[i], linewidth=1, linestyle='dashed', xmax=0.5)
    figure.add(n)

figure.ylabel = 'Reflectance $\\eta$ in $\\%$'
# figure.ax.text(25.8, 5, '$I_\\mathrm{th}$')

figure.xlabel = 'Increase in pump current $\\Delta I$ $(\\mathrm{mA})$'

ax.axhline(0, color='black', linewidth=1)
# ax.axvline(25.265, color='#8491B4', linewidth=1)

# yy = np.linspace(-50, 200, 2 * 8)
# ax.fill_betweenx(yy, 24.89, 25.32, linewidth=0, color='#8491B4', alpha=0.2)
ax.axhline(50, color='black', linewidth=1)

figure.plot()
ax.set_xlim(-7.5, 60)
ax.set_ylim(46.5, 53.5)
ax.set_xticks([0, 10, 20])
plt.legend(loc='right')
plt.savefig(f'reflectance.pdf')
