import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator


class Figure:

    y_scale = int(1e3)

    def __init__(self, width=0.6, residuals=True):
        self.xlabel = 'x'
        self.ylabel = 'y'
        self.plots = []
        self.x_major_locator = 5
        self.x_minor_locator = 1

        def mm_to_inch(x) -> float:
            return x / 25.4

        if width == 'single':
            fig_width = mm_to_inch(89)
            fig_height = fig_width / 4. * 3.
        elif width == 'double':
            fig_width = mm_to_inch(183)
            fig_height = fig_width / 16. * 9.
        else:
            fig_width = mm_to_inch(width)
            if width > 120:
                fig_height = fig_width / 16. * 9.
            elif width > 89:
                fig_height = fig_width / 4. * 3.
            else:
                fig_height = mm_to_inch(89) / 4. * 3.

        if residuals == True:
            self.fig, axs = plt.subplots(
                2,
                figsize=(fig_width, fig_height),
                gridspec_kw={'height_ratios': [2, 1]},
                sharex=True
            )
            self.ax = axs[0]
            self.ax_res = axs[1]
        else:
            self.fig, self.ax = plt.subplots(1, figsize=(fig_width, fig_height))

    def add(self, plot):
        self.plots.append(plot)

    def plot(self, label=None, fit_label=None):
        for data in self.plots:
            # Fit
            if data.fit is not None:
                self.ax.plot(
                    data.x_hr(),
                    data.fit/self.y_scale,
                    linewidth=1,
                    linestyle='dashed',
                    color=data.color,
                    label=fit_label
                )

        for data in self.plots:
            if label is None:
                label = data.label
            # Data
            self.ax.errorbar(
                data.x,
                data.y/self.y_scale,
                data.yerr/self.y_scale,
                linewidth=0,
                elinewidth=1,
                capsize=0,
                marker=data.marker,
                color=data.color,
                label=label
            )

            # Residuals
            if data.residuals is not None:
                self.ax_res.errorbar(
                    data.x,
                    data.residuals/self.y_scale,
                    data.yerr/self.y_scale,
                    linewidth=0,
                    elinewidth=1,
                    capsize=2,
                    marker=data.marker,
                    color=data.color
                )
                self.ax_res.axhline(0, linewidth=1, color='black')
                self.ax_res.set(xlabel=self.xlabel)
                self.ax_res.set(ylabel='Residuals')
            else:
                self.ax.set(xlabel=self.xlabel)

            if data.label is not None:
                self.ax.legend()

        # Axes
        self.ax.set_xlim([self.plots[0].xmin(), self.plots[0].xmax()])
        self.ax.xaxis.set_major_locator(MultipleLocator(self.x_major_locator))
        self.ax.xaxis.set_minor_locator(MultipleLocator(self.x_minor_locator))
        self.ax.set(ylabel=self.ylabel)
        self.fig.tight_layout()

    def add_theory(self, function):
        self.ax.plot(
            self.plots[0].x_hr(),
            function(self.plots[0].x_hr())/self.y_scale,
            linewidth=0.75,
            color='black'
        )
