import numpy as np
from scipy.optimize import curve_fit


class Data:

    x_resolution = int(2**13)
    x_overextension = 0.05

    def __init__(self, x, y, yerr=None, color='red', marker='.', label=None):
        self.x = x
        self.y = y
        self.yerr = yerr
        self.fit = None
        self.residuals = None
        self.popt = None
        self.pcov = None
        self.color = color
        self.marker = marker
        self.label = label

    def xmin(self):
        return self.x.min() - self.x_overextension * (self.x.max() - self.x.min())

    def xmax(self):
        return self.x.max() + self.x_overextension * (self.x.max() - self.x.min())

    def x_hr(self):
        return np.linspace(self.xmin(), self.xmax(), self.x_resolution)

    def regression(self, function, bounds):
        self.popt, self.pcov = curve_fit(
            function,
            self.x,
            self.y,
            sigma=self.yerr,
            bounds=bounds,
            absolute_sigma=True
        )
        self.fit = function(self.x_hr(), *self.popt)
        self.residuals = self.y - function(self.x, *self.popt)

    def error(self, i):
        return np.sqrt(self.pcov[i][i])

    def chi_square(self, a=0, b=None):
        if b is None:
            b = len(self.residuals) + 1
        return np.sum(self.residuals[a:b]**2/self.yerr[a:b]**2)

    def ndf(self, a=0, b=None):
        if b is None:
            b = len(self.residuals) + 1
        return len(self.residuals[a:b]) - len(self.popt)

    def chi_square_per_ndf(self, a=0, b=None):
        if b is None:
            b = len(self.residuals) + 1
        return self.chi_square(a, b) / self.ndf(a, b)
