import numpy as np
import matplotlib.pyplot as plt
from analyzer import Data, Figure


def x_to_tau(x):
    return x / 1e-6


def func_tau(tau, n_max=1000, n_min=200, d_omega=1.2, tau0=0):
    v_hom = (n_max - n_min) / n_max
    return n_max * (1 - v_hom * np.exp(-(d_omega*(tau - tau0))**2))


def func(x, n_max=1000, n_min=200, coherence_length=10, x0=6):
    return func_tau(x_to_tau(x), n_max, n_min, 1./x_to_tau(coherence_length), x_to_tau(x0))


# Read csv
csv = np.genfromtxt('data/5.4_HOM-coincidences-rates.csv', delimiter=';')
x = csv[2:, 0]
y = csv[2:, 1]
yerr = np.sqrt(y)
bounds = ([500, 0, 0, 4], [1500, 500, np.infty, 8])

# Regression analysis
n = Data(x, y, yerr=yerr, color='black')
n.regression(func, bounds=bounds)

# Statistics
print(f'N_max = {n.popt[0]:.0f} ± {n.error(0):.0f}')
print(f'N_min = {n.popt[1]:.0f} ± {n.error(1):.0f}')
print(f'lc = {n.popt[2]:.3f} ± {n.error(2):.3f}')
print(f'x0 = {n.popt[3]:.3f} ± {n.error(3):.1f}')
print(f'Chi2 = {n.chi_square():.2f}')
print(f'ndf = {n.ndf()}')
print(f'chi2/ndf = {n.chi_square_per_ndf()}')
N = n.popt[0]
M = n.popt[1]
Ne = n.error(0)
Me = n.error(1)
V = (N - M) / N
Verr = V * np.sqrt((Ne/(N-M))**2 + (Me/(N-M))**2 + (Ne/N)**2)
print(f'V = {V*100:.2f} ± {Verr*100:.2f}')

# Plot HOM-dip figure
Figure.y_scale = 1
figure = Figure(width='double')
figure.x_major_locator = 1
figure.ax.axhline(n.popt[0], linewidth=0.8, color='#CCCCCC', label='$N_{\\mathrm{max}} = 960 \\pm 15\\,\\mathrm{Hz}$')
figure.ax.vlines(n.popt[3], n.popt[1], n.popt[0], linewidth=0.8, color='#3C5488', label='$V_{\\mathrm{HOM}}\, N_{\\mathrm{max}}$')
figure.ax.hlines(670, n.popt[3] - n.popt[2], n.popt[3] + n.popt[2], linewidth=1.3, color='k', label='$2\\,l_\\mathrm{c} = 2\\,( 1.84 \\pm 0.07 )\\,\\mathrm{\mu m}$')
figure.add(n)
#figure.add_theory(func)

# Plot settings
figure.ylabel = 'Rate in Hz'
figure.xlabel = 'Position in $\\mu m$'
figure.plot(label='Data', fit_label='Fit result')
# figure.ax.legend(['Regression', 'Data'])
figure.ax.set_ylim([0, 1050])
figure.ax.legend()

plt.savefig('hom_dip.pdf')
