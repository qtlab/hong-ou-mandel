import numpy as np
import matplotlib.pyplot as plt
from analyzer import Data, Figure

ZOOM_PLOT = False
TIME_WINDOW = 30e-9


def func(current, chi=2500, lasing_threshold=25, m=4, b=1):
    return np.where(current < lasing_threshold, 0, chi * (current - lasing_threshold)) + m * current + b


def func_static(current, chi=2500, lasing_threshold=25, b=1):
    return np.where(current < lasing_threshold, 0, chi * (current - lasing_threshold)) + b


def func_quadratic(current, chi=2500, lasing_threshold=25, m=4):
    return np.where(current < lasing_threshold, 0, chi * (current - lasing_threshold)) + m * current ** 2


# Read csv
csv = np.genfromtxt('data/5.2_Counts-vs-Pumpcurrent.csv', delimiter=';')
x = csv[3:, 0]

ys = [
    csv[3:, 1],  # Detector 0
    csv[3:, 2],  # Detector 1
    csv[3:, 3] - csv[3:, 1]*csv[3:, 2]*TIME_WINDOW  # Coincidence
]
kwargs = [
    {'color': '#3C5488', 'label': 'Input 0'},
    {'color': '#00A087', 'label': 'Input 1'},
    {'color': 'black', 'label': 'Coincidence'}
]
bounds = ([1, 20, 0, 0], [np.infty, 30, np.infty, np.infty])
bounds_coincidence = ([1, 20, 0], [np.infty, 30, np.infty])

# Fit and plot characteristics
ds = []
figure = Figure(width='double')
for i in range(3):
    if i == 2:
        yerr = np.sqrt(ys[2] + (ys[0] + ys[1])*ys[0]*ys[1]*TIME_WINDOW**2)
        n = Data(x, ys[i], yerr=yerr, **kwargs[i])
        n.regression(func_quadratic, bounds=bounds_coincidence)
    else:
        n = Data(x, ys[i], yerr=np.sqrt(ys[i]), **kwargs[i])
        n.regression(func_static, bounds=bounds_coincidence)
    figure.add(n)
    ds.append(n)
    # This causes wrong errors in the upper plot
    # However, no errors are visible either way
    if ZOOM_PLOT is True:
        n.residuals[6:] = 0.1*n.residuals[6:]
        n.yerr[6:] = 0.1*n.yerr[6:]

ax = figure.ax_res
ax2 = ax.twinx()
ax.set_ylim(-0.55, 0.55)
ax2.set_ylim(-0.55, 0.55)
if ZOOM_PLOT is True:
    ax2.set_ylim(-5.5, 5.5)

# Split residuals into two halves
ax.axvline(27.5, color='black', linewidth=1)

figure.xlabel = 'Pump current $(\\mathrm{mA})$'
figure.ylabel = 'Incidence $(\\mathrm{kHz})$'

ax = figure.ax
# X axis
ax.axhline(0, color='black', linewidth=1)

# Critical current
ax.axvline(25.265, color='#8491B4', linewidth=1)
yy = np.linspace(-50, 200, 2*8)
ax.fill_betweenx(yy, 24.89, 25.32, linewidth=0, color='#8491B4', alpha=0.2)
ax.text(25.8, 150, '$I_\\mathrm{th} = 25.27\\,\\mathrm{mA}$')
ax.set_ylim(0, 170)

figure.plot()
ax.set_xlim(-0.5, 50.5)
plt.savefig('characteristics.pdf')

# Statistics
print('Detector 0')
print(f'χ = {ds[0].popt[0]:.0f} ± {ds[0].error(0):.0f}')
print(f'Threshold I = {ds[0].popt[1]:.3f} ± {ds[0].error(1):.3f}')
print(f'χ²/ndf = {ds[0].chi_square_per_ndf():.1f}')
print(f'χ² (cut) = {ds[0].chi_square(b=5):.1f}')
print()

print('Detector 1')
print(f'χ = {ds[1].popt[0]:.0f} ± {ds[1].error(0):.0f}')
print(f'Threshold I = {ds[1].popt[1]:.3f} ± {ds[1].error(1):.3f}')
print(f'χ²/ndf = {ds[1].chi_square_per_ndf():.1f}')
print(f'χ² (cut) = {ds[1].chi_square(b=5):.1f}')
print()

print('Coincidence')
print(f'χ = {ds[2].popt[0]:.1f} ± {ds[2].error(0):.1f}')
print(f'Threshold I = {ds[2].popt[1]:.3f} ± {ds[2].error(1):.3f}')
print(f'χ²/ndf = {ds[2].chi_square_per_ndf():.0f}')
print(f'χ² (cut) = {ds[2].chi_square(b=5):.1f}')
print()
